# Docker With Spring Boot and MySQL
_Proyecto para crear dos contenedores Docker, con una aplicacion Sprint Boot y consultando una BD Mysql_

## Comenzando 🚀

_git clone https://gitlab.com/alex.miranda.sanchez/book-manager_

_gradle https://gradle.org/_

Mira **Paso a paso** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Docker 

Java

Gradle

o Maven

Postman
```

### Paso a paso 🔧

_Manos a la obra para construir los contenedores y probar la aplicacion_

_Probando Docker_

```
docker --version

docker info

docker run hello-world

```

_Setup MySql_

```
docker run -d -p 6033:3306 --name=docker-mysql --env="MYSQL_ROOT_PASSWORD=root" --env="MYSQL_PASSWORD=root" --env="MYSQL_DATABASE=book_manager" mysql

docker image ls

docker container ls

docker exec -ti docker-mysql bash
    'mysql -uroot -proot'

docker exec -i docker-mysql mysql -uroot -proot book_manager <sql/book_manager.sql

docker exec -ti docker-mysql bash
    'mysql -uroot -proot'

```

_Crear el Dockerfile con el siguiente contenido_

```
FROM java:8
VOLUME /tmp 
EXPOSE 10222 
ADD ./build/libs/book-manager-1.0-SNAPSHOT.jar book-manager-1.0-SNAPSHOT.jar 
ENTRYPOINT ["java","-jar","book-manager-1.0-SNAPSHOT.jar"]

```

_Construir la imagen Docker_

```
docker build -f Dockerfile -t book_manager_app .

```

_Correr el contenedor y probar_

```
docker run -t --link docker-mysql:mysql -p 10222:10222 book_manager_app

http://localhost:10222/book

```

## Autores ✒️

_FYI_

* **Alex Miranda** - *Dev* - [alexmiranda](https://gitlab.com/alex.miranda.sanchez)